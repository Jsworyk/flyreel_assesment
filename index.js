// To run please intialize a MongoDb, can be done running the mongodamon command,
// then please run the necessary APIs to create Clients. I recommend postman for
// the runnning of the APIs. Use the unique IDs created by MongoDb for the API calls
// that require id parameters. Reference the User model ('./models/user) for the necessary fields that need
// to be passed in the body of the request to create users & tasks
// Made adding user and adding task a two part approach to accomodate the use case where we are adding a new user but they don't have a task yet.
// First call add user API then optionally call add task API passing tasks by User ID.

//To add client D to code base call 'http://localhost:{whateverportyoureon}/ using a POST call on PostMan passing in the clients name..

const express = require ('express');
const {User} = require ('./models/user');
const app = express ();
const mongoose = require ('mongoose');
const morgan = require ('morgan');

//Connect to MongoDb

mongoose
  .connect ('mongodb://localhost/flyreel')
  .then (() => console.log ('Connected to MongoDB'))
  .catch (err => {
    console.log (err.message);
  });

//Middleware
app.use (express.json ());
app.use (express.urlencoded ({extended: true}));
if (app.get ('env') === 'development') {
  app.use (morgan ('tiny'));
  console.log ('Morgan enabled...');
}

//Register API Routes
// I Intended to break these off into the './routes/users folder
// but experienced problems, after 2 minutes of 404 errors I decided
// i would spend my time better and create the routes as opposed to
// routing to them. In an enterprises application they would be broken
// off into the their own folders for each model... ie: users/ insurance
//  agencies...

const users = require ('./routes/users');
app.use ('api/users', users);

//All Routing down through index.js to save time... set up the environment hastily and
// didn't prioritize time to understand where i erred in the routing to the external routes file

//Add Users

app.post ('/', async (req, res) => {
  let user = new User (req.body);
  await user.save ();
  res.send ('User Saved');
});

//Get Users

app.get ('/', async (req, res) => {
  const users = await User.find ().sort ('name');
  res.send (users);
});

//Get User By ID

app.get ('/:id', async (req, res) => {
  const user = await User.findOne ({_id: req.params.id});
  if (!user) {
    res.status (404).send ('A User With The Provided ID Could Not Be Found');
    return;
  }
  res.send (user);
});

//Change users name By ID

app.put ('/:id/change_name', async (req, res) => {
  const user = await User.findOne ({_id: req.params.id});
  if (!user) {
    res.status (404).send ('User with the provided ID Could Not Be Found');
    return;
  }
  user.name = req.body.name;
  user.save ();
  res.send (user);
});

//Add Task To User By User ID

app.put ('/:id/add_task', async (req, res) => {
  const user = await User.findOne ({_id: req.params.id});
  if (!user) {
    res
      .status (400)
      .send ('A User With The Provided ID Could Not Be Retrieved');
    return;
  }

  const task = req.body;
  user.tasks.push (task);
  user.save ();
  res.send (user);
});

//Get Full ToDo List By User ID
//Client B Solution

app.get ('/:id/tasks', async (req, res) => {
  const user = await User.findOne ({_id: req.params.id});
  if (!user) {
    res
      .status (404)
      .send ('A User With The Provided ID Could Not Be Retrieved');
    return;
  }
  res.send ({tasks: user.tasks});
});

//Mark Task As Complete By USER ID and TASK ID
//:id = user ID
//:task = task ID

app.patch ('/:id/complete/:task', async (req, res) => {
  const user = await User.findOne ({_id: req.params.id});
  if (!user) {
    res.status (400).send ('A User with the specified ID could not be found');
    return;
  }
  User.update (
    {'tasks._id': req.params.task},
    {
      $set: {
        'tasks.$.complete': true,
      },
    },
    err => {
      if (err) {
        console.log (err.message);
      }
    }
  );
  user.save ();
  res.send (user);
});

//Mark Task as incomplete
//Use case: Perhaps your wife/gf was not content with the job you did cutting the lawn.... lol

app.patch ('/:id/incomplete/:task', async (req, res) => {
  const user = await User.findOne ({_id: req.params.id});
  if (!user) {
    res.status (400).send ('A User with the specified ID could not be found');
    return;
  }
  User.update (
    {'tasks._id': req.params.task},
    {
      $set: {
        'tasks.$.complete': false,
      },
    },
    err => {
      if (err) {
        console.log (err.message);
      }
    }
  );
  user.save ();
  res.send (user);
});

//Get 5 most recent to-dos by date listed.
//Client A Solution

app.get ('/:id/five_most_recent', async (req, res) => {
  const user = await User.findOne ({_id: req.params.id});
  if (!user) {
    res
      .status (400)
      .send ('A User With The Provided ID Could Not Be Retrieved');
    return;
  }
  let recentTasks = user.tasks;
  let fivetasks = recentTasks.slice (0, 5);
  res.send ({tasks: fivetasks});
});

//Return Incomplete tasks with title ONLY
//Client C Solution

app.get ('/:id/get_incomplete_tasks', async (req, res) => {
  const user = await User.findOne ({_id: req.params.id});
  if (!user) {
    res
      .status (400)
      .send ('A user with the provided ID could not be retrieved');
    return;
  }
  const tasks = user.tasks;
  const completeTasks = [];
  for (var i = 0; i < tasks.length; i++) {
    if (!tasks[i].complete) {
      completeTasks.push (tasks[i].task);
    }
  }
  res.send (completeTasks);
});

//Return complete tasks with title ONLY
//Client C Solution reciprocal

app.get ('/:id/complete/', async (req, res) => {
  const user = await User.findOne ({_id: req.params.id});
  if (!user) {
    res
      .status (400)
      .send ('A user with the provided ID could not be retrieved');
    return;
  }
  const tasks = user.tasks;
  const completeTasks = [];
  for (var i = 0; i < tasks.length; i++) {
    if (tasks[i].complete) {
      var paramKey = req.params.filtration;
      completeTasks.push (tasks[i].task);
    }
  }
  res.send (completeTasks);
});

app.listen (3000, console.log (`Listening on Port 3000`));
