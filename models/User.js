const mongoose = require ('mongoose');

const toDoSchema = new mongoose.Schema ({
  task: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  complete: {
    type: Boolean,
  },
});

const userSchema = new mongoose.Schema ({
  name: {
    type: String,
    required: true,
  },
  tasks: [toDoSchema],
});

const User = mongoose.model ('User', userSchema);

exports.User = User;
