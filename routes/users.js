const {User} = require ('../models/user');
const express = require ('express');
const router = express.Router ();

//Add Users - Solves the client D & E Problem
//To Add Client D Run this query in postman. Pass a name.

router.post ('/', async (req, res) => {
  let user = new User (req.body);
  await user.save ();
  res.send ('User Saved');
});

//Get Users

router.get ('/', async (req, res) => {
  const users = await User.find ().sort ('name');
  res.send (users);
});

//Get User By ID

router.get ('/:id', async (req, res) => {
  const user = await User.findOne ({_id: req.params.id});
  if (!user) {
    res.status (404).send ('A User With The Provided ID Could Not Be Found');
    return;
  }
  res.send (user);
});

//Add Task To User By User ID

router.put ('/:id/add_task', async (req, res) => {
  const user = await User.findOne ({_id: req.params.id});
  if (!user) {
    res
      .status (400)
      .send ('A User With The Provided ID Could Not Be Retrieved');
    return;
  }

  const task = req.body;
  user.tasks.push (task);
  user.save ();
  res.send (user);
});

//Get ToDo List By User ID

router.get ('/:id/tasks', async (req, res) => {
  const user = await User.findOne ({_id: req.params.id});
  if (!user) {
    res
      .status (404)
      .send ('A User With The Provided ID Could Not Be Retrieved');
    return;
  }
  res.send ({tasks: user.tasks});
});

module.exports = router;
